/*
Reading a n integer value from keyboard the program
must compute its prime factorization and write into
values.out file as it follows:
each pair of prime factor and its appearance in ascending order by appearance
or a specific message if the number it is already prime.

Keyboard value
16200

values.out
5 2
2 3
3 4
*/

#include<stdio.h>

typedef struct factorization
{
    int base, exponent;
}factorization;

void Compute(int *i, int number, factorization x[100]);
void Write(int i, int number, factorization x[100]);
void Sort(int factors, factorization x[100]);

int main()
{
    int n, factors_number;
    factorization a[100];

    factors_number=0;
    scanf("%i", &n);
    Compute(&factors_number, n, a);
    if(factors_number>1)
        Sort(factors_number, a);
    Write(factors_number, n, a);
    return 0;
}

void Compute(int *i, int number, factorization x[100])
{
    int factor, count;
    factor=2;
    count=0;
    while(number!=1)
    {
        count=0;
        while(number%factor==0)
        {
            x[*i].base=factor;
            number/=factor;
            count++;
        }
        if(count)
        {
            x[*i].exponent=count;
            (*i)++;
        }
        factor++;
    }
}

void Sort(int factors, factorization x[100])
{
    int i, j, aux1, aux2;
    for(i=0; i<factors-1; i++)
        for(j=i+1; j<factors; j++)
            if(x[i].exponent > x[j].exponent)
            {
                aux1=x[i].base;
                aux2=x[i].exponent;

                x[i].base=x[j].base;
                x[i].exponent=x[j].exponent;

                x[j].base=aux1;
                x[j].exponent=aux2;

            }
}

void Write(int factors_number, int number, factorization x[100])
{
    FILE *f;
    int index;

    f=fopen("values.out", "w");
    if(factors_number>1)
        for(index=0; index<factors_number; index++)
            fprintf(f, "%i %i\n", x[index].base, x[index].exponent);
    else
        fprintf(f, "%i it's a prime number!", number);
    fclose(f);
}
