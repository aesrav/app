/* Ascending sort of an array with n integer elements
using pointers. */

#include<stdio.h>
#include<stdlib.h>

int main()
{
    int i, n, A[15];
    read(&n, A);
    sort(&n, A);
    display(&n, A);
    return 0;
}

void read(int *n, int *A)
{
    int i;
    scanf("%i", n);
    for(i=0; i<(*n); i++)
        scanf("%i", A++);
}

void sort(int *n, int *A)
{
    int i, j, aux;
    for(i=0; i<((*n)-1); i++)
        for(j=i+1; j<(*n); j++)
            if((*(A+i))>(*(A+j)))
               {
                   aux=(*(A+i));
                   (*(A+i))=(*(A+j));
                   *(A+j)=aux;
               }
}

void display(int *n, int *A)
{
    int i;
    printf("\n");
    for(i=0; i<(*n); i++)
        printf("%i ", *(A+i));
    printf("\n");
}
