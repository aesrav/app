/*
Single-dimension array. Parameter passing. Statically allocation
*/

#include<stdio.h>
void readArray(int *size, int x[]);
void displayArray(int size, int x[]);

int main()
{
    int n;

    //the compiler will allocate a continuos block of 30 integer values index from 0 to 29 in the memory
    //without the possibility to free it when we don't need it anymore
    //accessing the 30th index element or a different bigger index value will return a "garbage" value
    int a[30];

    //we'll send the address of n and the address of first element of the array
    readArray(&n, a);

    displayArray(n, a);

    return 0;
}

void readArray(int *size, int *x) // due the way c it's built x[] ~ *x
{
    int i;

    //puts an integer value at address memorized by size which it's a pointer
    scanf("%i", size);

    for(i=0; i<*size; i++)
        scanf("%i", x+i); // due the way c it's built x[i] ~ *(x+i)
}

void displayArray(int size, int x[])
{
    int i;
    printf("\n");
    for(i=0; i<size; i++)
        printf("%i ", x[i]);
}


