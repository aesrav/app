/*
Given two integer arrays already ascending sorted the program
must merge them into a single ascending sorted array.

Input
n=6  2 3 8 9  11 17
m=7 -1 3 3 10 12 16 88

Output
p=13  -1 2 3 3 3 8 9 10 11 12 16 17 88
*/

#include<stdio.h>
int n, m, p, A[100], B[100], C[100];

void read();
void write();

int main()
{
    int x=0, a=0, b=0;
    read();

    while(x<(n+m))
    {
        if(A[a]<B[b])
            C[x++]=A[a++];
        else
            if(A[a]>B[b])
                C[x++]=B[b++];
            else
                if(A[a]==B[b])
                {
                    C[x++]=A[a++];
                    C[x++]=B[b++];
                }

        if(a==n)
            A[n]=B[m-1]+1;
        if(b==m)
            B[m]=A[n-1]+1;
    }

    write();
    return 0;
}

void write()
{
    int i;
    printf("\n");
    for(i=0; i<(n+m); i++)
        printf("%i ", C[i]);
}

void read()
{
    int i;
    scanf("%i %i", &n, &m);
    for(i=0; i<n; i++)
        scanf("%i", &A[i]);
    for(i=0; i<m; i++)
        scanf("%i", &B[i]);
}
