/*
Given as input a two-dimensional array the program must allocate dynamically
its nXm elements and print the number of zero elements from main diagonal and
the number of even numbers from secondary diagonal.

Input
3 3
0 1 3
2 1 7
4 0 5

Output
1
1
*/

#include<stdio.h>
#include<stdlib.h>

int **readArray(int *n, int *m)
{
    int i, j;
    scanf("%i %i", n, m);
    int **p=malloc(*n*sizeof(int*));
    if (p != NULL)
        for (i=0; i<*m; i++)
            *(p+i) =malloc(*n*sizeof(int));

    for(i=0; i<*n; i++)
        for(j=0; j<*m; j++)
            scanf("%i", (*(p+i)+j)); //equivalent with scanf("%i", &p[i][j]);

    return p;
}

void printArray(int n, int m, int **a)
{
    int i,j;
    for (i=0;i<n;i++)
    {
        for (j=0;j<m;j++)
            printf ("%3d", a[i][j]);
        printf("\n");
    }
}

int mainZerosCnt(int n, int **a)
{
    int i, nr=0;
    for(i=0; i<n; i++)
        if(a[i][i]==0)
            nr++;
    return nr;
}

int secEvenCnt(int n, int **a)
{
    int i, j, nr=0;
    for(i=0, j=n-1; i<n; i++, j--)
        if(a[i][j]%2==0)
            nr++;
    return nr;
}

int main ()
{
    int **a, n, m;
    a= readArray(&n, &m);
    printArray(n, m, a);
    printf("Number of zero elements on main diagonal: %i\n", mainZerosCnt(n, a));
    printf("Number of even numbers on secondary diagonal: %i", secEvenCnt(n, a));

    return 0;
}

