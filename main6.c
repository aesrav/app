/* The program must read from a file two complex numbers and
must calculate addition, substraction, multiplication, division, conjugate, or modulus.
The user must choose the specific operation.
Input
2 -1
3 7
*/

#include<stdio.h>

typedef struct complex
{
    float re, im;
}complex;

void Read(char *c, complex *a, complex *b);
void Addition(complex a, complex b);

int main()
{
    char option;
    complex z1, z2;
    Read(&option, &z1, &z2);
    switch(option)
    {
        case 'a':
            Addition(z1, z2);
            break;

        /*
        case 's':
            Substraction(z1, z2);
            break;
        case 'm':
            Multiplication(z1, z2);
            break;
        case 'd':
            Division(z1, z2);
            break;
        case 'c':
            Conjugate(z1, z2);
            break;
        case 'm':
            Modulus(z1, z2);
            break;
        */
        default:
            printf("Invalid option!\n");

    }
    return 0;
}

void Read(char *c, complex *a, complex *b)
{
    FILE *in;
    in=fopen("file.txt", "r");
    fscanf(in, "%c %f %f %f %f", c, &a->re, &a->im, &b->re, &b->im);
    fclose(in);
}

void Addition(complex a, complex b)
{
    printf("(%f+%fi)+(%f+%fi)=(%f+%fi)\n", a.re, a.im,
           b.re, b.im, a.re+b.re, a.im+b.im);
}
