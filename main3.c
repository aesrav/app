/* Getting as input a two-dimensional real values array
the program must display the value whith the biggest
frequency in the array.
If there are more than one then it must display the biggest value.

Input
3 4
-2   7.1 2.3 -1
7.1  1   2.3 -4
0    1   2.3  7.1

Output
7
*/

#include<stdio.h>
int n, m, length, count[100];
float value[100];

void read()
{
    int i, j, t, ok;
    float x;

    scanf("%i %i", &n, &m);
    for(i=0; i<n; i++)
    {
        for(j=0; j<m; j++)
        {
            scanf("%f", &x);

            ok=0;
            for(t=0; t<length; t++)
                if(x==value[t])
                {
                    count[t]++;
                    ok=1;
                    break;
                }

            //if x not found in value
            if(!ok)
                value[length++]=x; //add x at final of value
        }

    }
}


int main()
{
    int i, max; float output;
    read();
    max=count[0];
    output=value[0];
    for(i=1; i<length; i++)
        if(count[i]>=max)
        {
            max=count[i];
            if(value[i]>output)
                output=value[i];
        }
    printf("%f", output);
    return 0;
}




