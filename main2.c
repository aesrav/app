/* Getting as input from keyboard a two-dimensional array
the program must calculate the sum of elements for each line and
then display the lines in ascending order (from the smallest corresponding
sum line to the biggest).

Input
4 4
0 7 -1  3
2 1  1  0
5 4  2  8
1 0  0 -3

Output
3 1 0 2
*/

#include<stdio.h>

int n, m;
int sum[100]; //sum[i]=s, where "s" it's the sum of elements from line "i"
int line[100]; //array which will contain sortered lines

void read()
{
    int i, j, x, s;
    scanf("%i %i", &n, &m);
    for(i=0; i<n; i++)
    {
        s=0;
        for(j=0; j<m; j++)
        {
            scanf("%i", &x);
            s+=x;
        }
        sum[i]=s;
        line[i]=i;
    }

}


void sort()
{
    int i, j, aux;
    for(i=0; i<n-1; i++)
    {
        for(j=i+1; j<n; j++)
            if(sum[i]>sum[j])
            {
                aux=sum[i];
                sum[i]=sum[j];
                sum[j]=aux;

                aux=line[i];
                line[i]=line[j];
                line[j]=aux;
            }
    }
}

void show()
{
    int i;
    for(i=0; i<n; i++)
        printf("%i ", line[i]);
}

int main()
{
    read();
    sort();
    show();
    return 0;
}

