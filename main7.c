/*Reading from a file n pairs of three values the program must
compute and write to another file the values can form the sides of a triangle.*/

#include <stdio.h>
#include <stdlib.h>

typedef struct triangle
{
    float a, b, c;
}triangle;

void Read(int *number, triangle x[100]);
void Write(int number, triangle x[100]);
int It_Is_a_Triangle(triangle x);

int main()
{
    int n;
    triangle a[100];
    Read(&n, a);
    Write(n, a);
    return 0;
}

void Read(int *number, triangle x[100])
{
    FILE *f; int i;
    f=fopen("values.in", "r");
    fscanf(f, "%i", number);
    for(i=0; i<*number; i++)
        fscanf(f, "%f %f %f", &x[i].a, &x[i].b, &x[i].c);
}

void Write(int number, triangle x[100])
{
    int i; FILE *out;
    out=fopen("values.out", "w");
    for(i=0; i<number; i++)
        if(It_Is_a_Triangle(x[i]))
            fprintf(out, "%f %f %f", x[i].a, x[i].b, x[i].c);
    fclose(out);
}

int It_Is_a_Triangle(triangle x)
{
    if(x.b + x.c > x.a && x.a + x.c > x.b && x.a + x.b > x.c)
        return 1;
    return 0;
}

